# Random Number Engines

This project implements a number of random number generators of th xoroshiro family. They are STL compatible and can serve as
drop-in replacements for the STL generators in the C++ `<random>` header. Implementation is in C++17

## Installation

This package requires a recent installation of [CMake][cmake]
and a C++ compiler supporting C++17. On macOS, the Apple Clang is installed
if you activated the command line tools, and it should get the job done.

1. Enter the source directory, create a build directory, and enter it:

        mkdir build
        cd build

2. Call CMake on the source directory:

        cmake -DCMAKE_INSTALL_PREFIX=<path> ..

    Replace `<path>` with the path where you want to install the library to.
    If omitted, this will be `/usr/local`. However, it is recommended to choose
    another path as `/usr/local` is usually (and should _only_ be!) managed
    by a package manager.

3. Build and install the `multirand` library:

        make && make install

### Usage in other modules

After installing the package, it can be found by other projects if your find
paths are configured correctly. Usually, it should suffice to do the following:

```cmake
find_package(RandomNumberEngines [<VERSION>])
target_link_libraries(<target> RandomNumberEngines::multirand)
```
Insert the required version for `<VERSION>` and replace `<target>` with the
target of your project you wish to link to this library.

If this project is not found, you can add

    -DRandomNumberEngines_ROOT=<path>

to your call to CMake, where you replace `<path>` with the earlier install path.

In your source code, you can simply include the appropriate header:

```c++
#include <random_number_engines.hh>
```

### Running tests

This module includes a set of tests. After configuring the module, enter the
`test` directory inside the _build_ directory. For compiling all tests, execute

    make all

and then run the tests with

    make test


### Important Remarks
The tests are carried out via a chi-square test for uniformity and normality, 
using all generators with the respective distribution functions and with different seeds.
Since these are probabilistic tests, some are expected to fail by chance. 
The limit for the number of fails is currently set to 15/100, with the '15' being
derived from experience. Above 15 fails, the test is considered to fail. 
Typical failure levels are between 0 and 11, 
with 11 occurring only for Linear Congruential/mt19937 (being from the c++ stdlib)
or for Xorshift1024* and Ran3, depending on the compiler.

Additionally, a plotting script in Python has been provided which shows histograms
and spectral-test plots for visual inspection. 
In order to use it, a 'data' folder has to be provided in the 'test' directory 
in which the data to plot can be stored. After executing the script with 
``` Python
    python3 plot.py
```

the resulting plots can be found in the folder test/plots


[cmake]: https://cmake.org/
