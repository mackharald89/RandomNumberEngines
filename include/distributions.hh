#ifndef MULTIRAND_DISTRIBUTIONS_HH
#define MULTIRAND_DISTRIBUTIONS_HH
#include <limits>
#include <type_traits>

namespace MultiRand
{
// implementing distributions ourselves in order to ensure consistent test
// results for multiple platforms

/**
 * @brief uniform real number distribution functor
 *
 * @tparam T
 * @tparam 0
 */
template < typename T,
           std::enable_if_t< std::is_floating_point< T >::value, int > = 0 >
class uniform_real_distribution
{
  private:
    T _start;
    T _end;

  public:
    /**
     * @brief Make uniformly distributed random number in [start, end) (mind the
     * ')'! )
     *
     * @tparam Generator
     * @param generator
     * @return T
     */
    template < typename Generator >
    T
    operator()(Generator& generator)
    {
        return (static_cast< T >(generator()) / Generator::max()) *
                   (_end - _start) +
               _start;
    }
    /**
     * @brief Construct a new uniform int distribution object
     *
     * @param start
     * @param end
     */
    uniform_real_distribution(T start, T end) : _start(start), _end(end)
    {
    }
};

/**
 * @brief uniform integer distribution functor
 *
 * @tparam T
 * @tparam 0
 */
template < typename T,
           std::enable_if_t< std::is_integral< T >::value, int > = 0 >
class uniform_int_distribution
{
  private:
    T _start;
    T _end;

  public:
    /**
     * @brief Make uniformly distributed random number in [start, end]
     *
     * @tparam RNG
     * @param rng random number generator
     * @return T
     */
    template < typename Generator >
    T
    operator()(Generator& generator)
    {
        return static_cast< T >((generator()) % (_end - _start + 1)) + _start;
    }
    /**
     * @brief Construct a new uniform int distribution object
     *
     * @param start
     * @param end
     */
    uniform_int_distribution(T start, T end) : _start(start), _end(end)
    {
    }
};

/**
 * @brief Normal distribution functor
 *
 * @tparam T
 * @tparam 0
 */
template < typename T,
           std::enable_if_t< std::is_floating_point< T >::value, int > = 0 >
class normal_distribution
{
  private:
    T _mean;
    T _stddev;

  public:
    /**
     * @brief Make normal-distributed random number from RNG output -> N(mean,
     * stddev)
     *
     * @tparam RNG
     * @param rng random number generator to use
     * @return T normal distributed random number
     */
    template < typename Generator >
    inline T
    operator()(Generator& generator)
    {
        double x    = 0.;
        double y    = 0.;
        double s    = 0.;
        auto   dist = uniform_real_distribution(-1., 1.);
        while (s > 1.0 - std::numeric_limits< double >::epsilon() ||
               std::abs(s) < 1e-16)
        {
            x = dist(generator);
            y = dist(generator);
            s = x * x + y * y;
        }
        return _mean + _stddev * x * std::sqrt(-2. * std::log(s) / s);
    }

    /**
     * @brief Construct a new normal distribution object
     *
     * @param mean
     * @param stddev
     */
    normal_distribution(T mean, T stddev) : _mean(mean), _stddev(stddev)
    {
    }
};
} // namespace MultiRand

#endif
