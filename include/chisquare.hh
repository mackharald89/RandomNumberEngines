#ifndef MULTIRAND_CHISQUARE_HH
#define MULTIRAND_CHISQUARE_HH
#include <cmath>
#include <iostream>
#include <map>
#include <vector>

namespace MultiRand
{
/**
 * @brief Class for χ^2 test for testing distributions
 *
 */
class ChiSquareTest
{
  private:
    // kahan summation for accuracy
    template < typename InputIterator, typename F >
    static double
    __sum_kahan__(InputIterator start, InputIterator end, F f)
    {
        if (start == end)
        {
            return 0.;
        }
        // auto size = std::distance(start, end);
        double comp = 0.;
        double sm   = 0.;
        for (; start != end; ++start)
        {
            double y = f(*start) - comp;
            double t = sm + y;
            comp     = (t - sm) - y;
            sm       = t;
        }
        return sm;
    }

    // lookup table for quantiles
    static std::map< std::pair< int, double >, double > _quantile_values;

  public:
    // first value in pair is expected_value. second value in pair is test_value
    static double
    testquantity(std::vector< std::pair< std::size_t, std::size_t > > data)
    {
        return __sum_kahan__(
            data.begin(), data.end(), [](auto& pair) -> double {
                double expected = pair.first;
                double observed = pair.second;

                if (expected == 0)
                {
                    return 0.;
                }
                else
                {
                    return std::pow((observed - expected), 2) /
                           (expected); // T statistics?
                }
            });
    }

    // evaluate test with given confidence interval
    static bool
    evaluate(std::vector< std::pair< std::size_t, std::size_t > > data,
             double desired_confidence,
             int    dofs)
    {
        if (dofs > 13)
        {
            std::cerr << "Cannot do chisquare test for more than 13 degrees of "
                         "freedom  "
                         "because quantile values are not provided, got " +
                             std::to_string(dofs) + " degrees of freedom!";
            return false;
        }
        else
        {
            double tq  = testquantity(data);
            auto   key = std::pair< int, double >{ dofs, desired_confidence };
            return tq < _quantile_values[key];
        }
    }
}; // namespace MultiRandclassChiSquareTest

// initialize lookup table for quantiles: {#dofs, quantile, value}
// taken from wikipedia
std::map< std::pair< int, double >, double > ChiSquareTest::_quantile_values = {
    { std::pair< int, double >{ 1, 0.900 }, 2.71 },
    { std::pair< int, double >{ 1, 0.950 }, 3.84 },
    { std::pair< int, double >{ 1, 0.975 }, 5.02 },
    { std::pair< int, double >{ 1, 0.990 }, 6.63 },
    { std::pair< int, double >{ 1, 0.995 }, 7.88 },
    { std::pair< int, double >{ 1, 0.999 }, 10.83 },
    { std::pair< int, double >{ 2, 0.900 }, 4.61 },
    { std::pair< int, double >{ 2, 0.950 }, 5.99 },
    { std::pair< int, double >{ 2, 0.975 }, 7.38 },
    { std::pair< int, double >{ 2, 0.990 }, 9.21 },
    { std::pair< int, double >{ 2, 0.995 }, 10.60 },
    { std::pair< int, double >{ 2, 0.999 }, 13.82 },
    { std::pair< int, double >{ 3, 0.900 }, 6.25 },
    { std::pair< int, double >{ 3, 0.950 }, 7.81 },
    { std::pair< int, double >{ 3, 0.975 }, 9.35 },
    { std::pair< int, double >{ 3, 0.990 }, 11.34 },
    { std::pair< int, double >{ 3, 0.995 }, 12.84 },
    { std::pair< int, double >{ 3, 0.999 }, 16.27 },

    { std::pair< int, double >{ 4, 0.900 }, 7.78 },
    { std::pair< int, double >{ 4, 0.950 }, 9.49 },
    { std::pair< int, double >{ 4, 0.975 }, 11.14 },
    { std::pair< int, double >{ 4, 0.990 }, 13.28 },
    { std::pair< int, double >{ 4, 0.995 }, 14.86 },
    { std::pair< int, double >{ 4, 0.999 }, 18.47 },

    { std::pair< int, double >{ 5, 0.900 }, 9.24 },
    { std::pair< int, double >{ 5, 0.950 }, 11.07 },
    { std::pair< int, double >{ 5, 0.975 }, 12.83 },
    { std::pair< int, double >{ 5, 0.990 }, 15.09 },
    { std::pair< int, double >{ 5, 0.995 }, 16.75 },
    { std::pair< int, double >{ 5, 0.999 }, 20.52 },

    { std::pair< int, double >{ 6, 0.900 }, 10.64 },
    { std::pair< int, double >{ 6, 0.950 }, 12.59 },
    { std::pair< int, double >{ 6, 0.975 }, 14.45 },
    { std::pair< int, double >{ 6, 0.990 }, 16.81 },
    { std::pair< int, double >{ 6, 0.995 }, 18.55 },
    { std::pair< int, double >{ 6, 0.999 }, 22.46 },

    { std::pair< int, double >{ 7, 0.900 }, 12.02 },
    { std::pair< int, double >{ 7, 0.950 }, 14.07 },
    { std::pair< int, double >{ 7, 0.975 }, 16.01 },
    { std::pair< int, double >{ 7, 0.990 }, 18.48 },
    { std::pair< int, double >{ 7, 0.995 }, 20.28 },
    { std::pair< int, double >{ 7, 0.999 }, 24.32 },

    { std::pair< int, double >{ 8, 0.900 }, 13.36 },
    { std::pair< int, double >{ 8, 0.950 }, 15.51 },
    { std::pair< int, double >{ 8, 0.975 }, 17.53 },
    { std::pair< int, double >{ 8, 0.990 }, 20.09 },
    { std::pair< int, double >{ 8, 0.995 }, 21.95 },
    { std::pair< int, double >{ 8, 0.999 }, 26.12 },

    { std::pair< int, double >{ 9, 0.900 }, 14.68 },
    { std::pair< int, double >{ 9, 0.950 }, 16.92 },
    { std::pair< int, double >{ 9, 0.975 }, 19.02 },
    { std::pair< int, double >{ 9, 0.990 }, 21.67 },
    { std::pair< int, double >{ 9, 0.995 }, 23.59 },
    { std::pair< int, double >{ 9, 0.999 }, 27.88 },

    { std::pair< int, double >{ 10, 0.900 }, 15.99 },
    { std::pair< int, double >{ 10, 0.950 }, 18.31 },
    { std::pair< int, double >{ 10, 0.975 }, 20.48 },
    { std::pair< int, double >{ 10, 0.990 }, 23.21 },
    { std::pair< int, double >{ 10, 0.995 }, 25.19 },
    { std::pair< int, double >{ 10, 0.999 }, 29.59 },

    { std::pair< int, double >{ 11, 0.900 }, 17.28 },
    { std::pair< int, double >{ 11, 0.950 }, 19.68 },
    { std::pair< int, double >{ 11, 0.975 }, 21.92 },
    { std::pair< int, double >{ 11, 0.990 }, 24.72 },
    { std::pair< int, double >{ 11, 0.995 }, 26.76 },
    { std::pair< int, double >{ 11, 0.999 }, 31.26 },

    { std::pair< int, double >{ 12, 0.900 }, 18.55 },
    { std::pair< int, double >{ 12, 0.950 }, 21.03 },
    { std::pair< int, double >{ 12, 0.975 }, 23.34 },
    { std::pair< int, double >{ 12, 0.990 }, 26.22 },
    { std::pair< int, double >{ 12, 0.995 }, 28.30 },
    { std::pair< int, double >{ 12, 0.999 }, 32.91 },

    { std::pair< int, double >{ 13, 0.900 }, 19.81 },
    { std::pair< int, double >{ 13, 0.950 }, 22.36 },
    { std::pair< int, double >{ 13, 0.975 }, 24.74 },
    { std::pair< int, double >{ 13, 0.990 }, 27.69 },
    { std::pair< int, double >{ 13, 0.995 }, 29.82 },
    { std::pair< int, double >{ 13, 0.999 }, 34.53 },
};
} // namespace MultiRand
#endif
