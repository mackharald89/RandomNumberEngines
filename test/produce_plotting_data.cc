#include <fstream>
#include <iomanip>
#include <iostream>
#include <random>
#include <random_number_engines.hh>

int
main()
{
    /**
     * @brief Makes 50000 random numbers for uniform real, integer distributions
     * and for normal distributions and saves them to file. For making plots
     * basically
     *
     */

    std::cout << "make_and_safe_data" << std::endl;
    short       seed       = 15243;
    std::size_t samplesize = 3 * 50000;

    std::mt19937         mtgen(seed);
    std::minstd_rand     msr(seed);
    XorShift128plus      xorshit128plus(seed);
    XorShift1024star     xorshit1024star(seed);
    SplitMix64           splitmix64(seed);
    Xoroshiro128starstar xoroshiro128starstar(seed);
    Xoroshiro128plus     xoroshiro128plus(seed);
    Xoroshiro256starstar xoroshiro256starstar(seed);
    Xoroshiro256plus     xoroshiro256plus(seed);
    Xoroshiro512starstar xoroshiro512starstar(seed);
    Xoroshiro512plus     xoroshiro512plus(seed);
    LinearCongruential<> linearcongruential(seed);
    Ran2                 ran2(seed);
    Ran3                 ran3(seed);

    uniform_int_distribution< int >     idist(-4, 4);
    uniform_real_distribution< double > rdist(-4., 4.);
    normal_distribution< double >       ndist(0., 1.);

    std::ofstream rdatafile(
        "../../test/data/rdata_" +
            (POSTFIX == 'G' ? std::string("gcc") : std::string("clang")) +
            ".csv",
        std::ios::out);
    std::ofstream idatafile(
        "../../test/data/idata_" +
            (POSTFIX == 'G' ? std::string("gcc") : std::string("clang")) +
            ".csv",
        std::ios::out);
    std::ofstream ndatafile(
        "../../test/data/ndata_" +
            (POSTFIX == 'G' ? std::string("gcc") : std::string("clang")) +
            ".csv",
        std::ios::out);

    rdatafile
        << "mt19937,minstd_rand,XorShift128plus,XorShift1024star,SplitMix64,"
           "Xoroshiro128starstar,Xoroshiro128plus,"
           "Xoroshiro256starstar,Xoroshiro256plus,Xoroshiro512starstar,"
           "Xoroshiro512plus,LinearCongruential,Ran2,Ran3"
        << std::endl;

    idatafile
        << "mt19937,minstd_rand,XorShift128plus,XorShift1024star,SplitMix64,"
           "Xoroshiro128starstar,Xoroshiro128plus,"
           "Xoroshiro256starstar,Xoroshiro256plus,Xoroshiro512starstar,"
           "Xoroshiro512plus,LinearCongruential,Ran2,Ran3"
        << std::endl;

    ndatafile
        << "mt19937,minstd_rand,XorShift128plus,XorShift1024star,SplitMix64,"
           "Xoroshiro128starstar,Xoroshiro128plus,"
           "Xoroshiro256starstar,Xoroshiro256plus,Xoroshiro512starstar,"
           "Xoroshiro512plus,LinearCongruential,Ran2,Ran3"
        << std::endl;
    std::vector< std::vector< double > > rdata(14);
    std::vector< std::vector< int > >    idata(14);
    std::vector< std::vector< double > > ndata(14);

    for (std::size_t j = 0; j < 14; ++j)
    {
        rdata[j].resize(samplesize);
        idata[j].resize(samplesize);
        ndata[j].resize(samplesize);
    }

    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[MTGEN][i]           = rdist(mtgen);
        rdata[MSR][i]             = rdist(msr);
        rdata[XOR128PLUS][i]      = rdist(xorshit128plus);
        rdata[XOR1024STAR][i]     = rdist(xorshit1024star);
        rdata[SPLITMIX64][i]      = rdist(splitmix64);
        rdata[XORO128STARSTAR][i] = rdist(xoroshiro128starstar);
        rdata[XORO128PLUS][i]     = rdist(xoroshiro128plus);
        rdata[XORO256STARSTAR][i] = rdist(xoroshiro256starstar);
        rdata[XORO256PLUS][i]     = rdist(xoroshiro256plus);
        rdata[XORO512STARSTAR][i] = rdist(xoroshiro512starstar);
        rdata[XORO512PLUS][i]     = rdist(xoroshiro512plus);
        rdata[LINCONG][i]         = rdist(linearcongruential);
        rdata[RAN2][i]            = rdist(ran2);
        rdata[RAN3][i]            = rdist(ran3);

        idata[MTGEN][i]           = idist(mtgen);
        idata[MSR][i]             = idist(msr);
        idata[XOR128PLUS][i]      = idist(xorshit128plus);
        idata[XOR1024STAR][i]     = idist(xorshit1024star);
        idata[SPLITMIX64][i]      = idist(splitmix64);
        idata[XORO128STARSTAR][i] = idist(xoroshiro128starstar);
        idata[XORO128PLUS][i]     = idist(xoroshiro128plus);
        idata[XORO256STARSTAR][i] = idist(xoroshiro256starstar);
        idata[XORO256PLUS][i]     = idist(xoroshiro256plus);
        idata[XORO512STARSTAR][i] = idist(xoroshiro512starstar);
        idata[XORO512PLUS][i]     = idist(xoroshiro512plus);
        idata[LINCONG][i]         = idist(linearcongruential);
        idata[RAN2][i]            = idist(ran2);
        idata[RAN3][i]            = idist(ran3);

        ndata[MTGEN][i]           = ndist(mtgen);
        ndata[MSR][i]             = ndist(msr);
        ndata[XOR128PLUS][i]      = ndist(xorshit128plus);
        ndata[XOR1024STAR][i]     = ndist(xorshit1024star);
        ndata[SPLITMIX64][i]      = ndist(splitmix64);
        ndata[XORO128STARSTAR][i] = ndist(xoroshiro128starstar);
        ndata[XORO128PLUS][i]     = ndist(xoroshiro128plus);
        ndata[XORO256STARSTAR][i] = ndist(xoroshiro256starstar);
        ndata[XORO256PLUS][i]     = ndist(xoroshiro256plus);
        ndata[XORO512STARSTAR][i] = ndist(xoroshiro512starstar);
        ndata[XORO512PLUS][i]     = ndist(xoroshiro512plus);
        ndata[LINCONG][i]         = ndist(linearcongruential);
        ndata[RAN2][i]            = ndist(ran2);
        ndata[RAN3][i]            = ndist(ran3);
    }

    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdatafile << rdata[MTGEN][i] << "," << rdata[MSR][i] << ","
                  << rdata[XOR128PLUS][i] << "," << rdata[XOR1024STAR][i] << ","
                  << rdata[SPLITMIX64][i] << "," << rdata[XORO256STARSTAR][i]
                  << "," << rdata[XORO128STARSTAR][i] << ","
                  << rdata[XORO128PLUS][i] << "," << rdata[XORO256PLUS][i]
                  << "," << rdata[XORO512STARSTAR][i] << ","
                  << rdata[XORO512PLUS][i] << "," << rdata[LINCONG][i] << ","
                  << rdata[RAN2][i] << "," << rdata[RAN3][i] << std::endl;

        idatafile << idata[MTGEN][i] << "," << idata[MSR][i] << ","
                  << idata[XOR128PLUS][i] << "," << idata[XOR1024STAR][i] << ","
                  << idata[SPLITMIX64][i] << "," << idata[XORO256STARSTAR][i]
                  << "," << idata[XORO128STARSTAR][i] << ","
                  << idata[XORO128PLUS][i] << "," << idata[XORO256PLUS][i]
                  << "," << idata[XORO512STARSTAR][i] << ","
                  << idata[XORO512PLUS][i] << "," << idata[LINCONG][i] << ","
                  << idata[RAN2][i] << "," << idata[RAN3][i] << std::endl;

        ndatafile << ndata[MTGEN][i] << "," << ndata[MSR][i] << ","
                  << ndata[XOR128PLUS][i] << "," << ndata[XOR1024STAR][i] << ","
                  << ndata[SPLITMIX64][i] << "," << ndata[XORO256STARSTAR][i]
                  << "," << ndata[XORO128STARSTAR][i] << ","
                  << ndata[XORO128PLUS][i] << "," << ndata[XORO256PLUS][i]
                  << "," << ndata[XORO512STARSTAR][i] << ","
                  << ndata[XORO512PLUS][i] << "," << ndata[LINCONG][i] << ","
                  << ndata[RAN2][i] << "," << ndata[RAN3][i] << std::endl;
    }
    return 0;
}
