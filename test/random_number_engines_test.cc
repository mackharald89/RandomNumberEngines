#if defined(__clang__)
#define POSTFIX 'C'
#elif defined(__GNUC__) || defined(__GNUG__)
#define POSTFIX 'G'
#endif

#include "../include/chisquare.hh"
#include "../test/test_utils.hh"
#include <chrono>
#include <cmath>
#include <distributions.hh>
#include <iostream>
#include <map>
#include <numeric>
#include <random>
#include <random_number_engines.hh>
#include <type_traits>
#include <vector>

using namespace MultiRand;
using namespace MultiRand::Testutils;
/**
 * @brief Execute chisquare test on different sample histograms. The latter are
 *        expected to be in the form of std::map<int, std::size_t> where the
 *        first template arg names the lower bounds of the intervals of the
 * histogram, and the second names the count for the bin.
 *
 *
 * @param test ChiSquareTest object
 * @param data vector which containes pairs of <expected bincount, actual
 * bincount>
 * @param ibuckets integer histogram map
 * @param rbuckets real value histogram map
 * @param nbuckets normal distributed histogram map
 * @param samplesize samplesize used
 * @param i iterator variable for indexing buckets vectors and names maps
 * @param failures: Lookup dictionary for (generator_name, distribution_kind)
 * @param test_confidence: 1 - alpha value for chi-squared test, e.g. 0.950
 * failure count
 */
template < typename T, typename U, typename Comp >
void
execute_test(ChiSquareTest&                                         test,
             std::vector< std::pair< std::size_t, std::size_t > >&  data,
             std::vector< std::map< T, std::size_t > >&             ibuckets,
             std::vector< std::map< U, std::size_t, Comp > >&       rbuckets,
             std::vector< std::map< U, std::size_t, Comp > >&       nbuckets,
             std::size_t                                            samplesize,
             int                                                    i,
             std::map< std::string, std::map< std::string, int > >& failures,
             double test_confidence)
{
    data.resize(ibuckets[i].size());
    for (auto [it, vit] = std::make_pair(ibuckets[i].begin(), data.begin());
         it != ibuckets[i].end();
         ++it, ++vit)
    {
        *vit = std::make_pair(samplesize / ibuckets[i].size(), it->second);
    }

    // actual tests

    if (test.evaluate(data, test_confidence, ibuckets[i].size() - 1) == false)
    {
        failures[MultiRand::Testutils::names[i]]["integer"] += 1;
    }
    data.clear();

    data.resize(rbuckets[i].size());
    for (auto [it, vit] = std::make_pair(rbuckets[i].begin(), data.begin());
         it != rbuckets[i].end();
         ++it, ++vit)
    {
        *vit = std::make_pair(samplesize / rbuckets[i].size(), it->second);
    }

    if (test.evaluate(data, test_confidence, rbuckets[i].size() - 1) == false)
    {
        failures[MultiRand::Testutils::names[i]]["real"] += 1;
    }
    data.clear();

    data.resize(nbuckets[i].size());
    for (auto [it, vit] = std::make_pair(nbuckets[i].begin(), data.begin());
         it != nbuckets[i].end();
         ++it, ++vit)
    {
        int num = std::round(
            P_interval_gauss(it->first - 0.5, it->first + 0.5) * samplesize);

        *vit = std::make_pair(num, it->second);
    }

    if (test.evaluate(data, test_confidence, nbuckets[i].size() - 1) == false)
    {
        failures[MultiRand::Testutils::names[i]]["normal"] += 1;
    }
    data.clear();
}

/**
 * @brief Actual test, tests if uniformity of resulting histogram when using
 *       the custom rngs passes a χ^2 test with 5% confidence -> ok?
 *
 */
void
test_chisquare()
{
    ChiSquareTest test;
    // test for all generators
    std::size_t                            testnum         = 100;
    std::size_t                            samplesize      = 1000000;
    double                                 test_confidence = 0.95;
    std::random_device                     rd;
    std::default_random_engine             eng(rd());
    std::uniform_int_distribution< short > seed_dist(
        0, std::numeric_limits< short >::max());

    std::map< std::string, std::map< std::string, int > > failures;
    for (auto& [i, name] : MultiRand::Testutils::names)
    {
        failures[name] = std::map< std::string, int >{ { "integer", 0 },
                                                       { "real", 0 },
                                                       { "normal", 0 } };
    }
    for (std::size_t l = 0; l < testnum; ++l)
    {
        std::cout << "currently at test number: " << l << std::endl;
        short seed = seed_dist(eng);

        // think we should have a new rng for each type of dist we are checking
        std::mt19937         mtgen_i(seed); // this is our primary touchstone because it is the de-facto standard
        std::minstd_rand     msr_i(seed);
        XorShift128plus      xorshit128plus_i(seed);
        XorShift1024star     xorshit1024star_i(seed);
        Xoroshiro128starstar xoroshiro128starstar_i(seed);
        Xoroshiro128plus     xoroshiro128plus_i(seed);
        Xoroshiro256starstar xoroshiro256starstar_i(seed);
        Xoroshiro256plus     xoroshiro256plus_i(seed);
        Xoroshiro512starstar xoroshiro512starstar_i(seed);
        Xoroshiro512plus     xoroshiro512plus_i(seed);
        LinearCongruential<> linearcongruential_i(seed);
        Ran3                 ran3_i(seed);

        std::mt19937         mtgen_r(seed);
        std::minstd_rand     msr_r(seed);
        XorShift128plus      xorshit128plus_r(seed);
        XorShift1024star     xorshit1024star_r(seed);
        Xoroshiro128starstar xoroshiro128starstar_r(seed);
        Xoroshiro128plus     xoroshiro128plus_r(seed);
        Xoroshiro256starstar xoroshiro256starstar_r(seed);
        Xoroshiro256plus     xoroshiro256plus_r(seed);
        Xoroshiro512starstar xoroshiro512starstar_r(seed);
        Xoroshiro512plus     xoroshiro512plus_r(seed);
        LinearCongruential<> linearcongruential_r(seed);
        Ran3                 ran3_r(seed);

        std::mt19937         mtgen_n(seed);
        std::minstd_rand     msr_n(seed);
        XorShift128plus      xorshit128plus_n(seed);
        XorShift1024star     xorshit1024star_n(seed);
        Xoroshiro128starstar xoroshiro128starstar_n(seed);
        Xoroshiro128plus     xoroshiro128plus_n(seed);
        Xoroshiro256starstar xoroshiro256starstar_n(seed);
        Xoroshiro256plus     xoroshiro256plus_n(seed);
        Xoroshiro512starstar xoroshiro512starstar_n(seed);
        Xoroshiro512plus     xoroshiro512plus_n(seed);
        LinearCongruential<> linearcongruential_n(seed);
        Ran3                 ran3_n(seed);

        uniform_int_distribution< int >     idist(-4, 4);
        uniform_real_distribution< double > rdist(-4., 4.);
        normal_distribution< double >       ndist(0., 1.);

        std::vector< std::vector< double > > rdata(14);
        std::vector< std::vector< int > >    idata(14);
        std::vector< std::vector< double > > ndata(14);

        std::vector< std::map< int, std::size_t > > ibuckets(14);
        std::vector< std::map< double, std::size_t, own_double_less > >
            rbuckets(14);
        std::vector< std::map< double, std::size_t, own_double_less > >
            nbuckets(14);

        std::vector< std::pair< std::size_t, std::size_t > > data;

        for (std::size_t i = 0; i < samplesize; ++i)
        {
            ++rbuckets[MTGEN][std::floor(rdist(mtgen_r))];
            ++rbuckets[MSR][std::floor(rdist(msr_r))];
            ++rbuckets[XOR128PLUS][std::floor(rdist(xorshit128plus_r))];
            ++rbuckets[XOR1024STAR][std::floor(rdist(xorshit1024star_r))];
            ++rbuckets[XORO128STARSTAR]
                      [std::floor(rdist(xoroshiro128starstar_r))];
            ++rbuckets[XORO128PLUS][std::floor(rdist(xoroshiro128plus_r))];
            ++rbuckets[XORO256STARSTAR]
                      [std::floor(rdist(xoroshiro256starstar_r))];
            ++rbuckets[XORO256PLUS][std::floor(rdist(xoroshiro256plus_r))];
            ++rbuckets[XORO512STARSTAR]
                      [std::floor(rdist(xoroshiro512starstar_r))];
            ++rbuckets[XORO512PLUS][std::floor(rdist(xoroshiro512plus_r))];
            ++rbuckets[LINCONG][std::floor(rdist(linearcongruential_r))];
            ++rbuckets[RAN3][std::floor(rdist(ran3_r))];

            ++ibuckets[MTGEN][idist(mtgen_i)];
            ++ibuckets[MSR][idist(msr_i)];
            ++ibuckets[XOR128PLUS][idist(xorshit128plus_i)];
            ++ibuckets[XOR1024STAR][idist(xorshit1024star_i)];
            ++ibuckets[XORO128STARSTAR][idist(xoroshiro128starstar_i)];
            ++ibuckets[XORO128PLUS][idist(xoroshiro128plus_i)];
            ++ibuckets[XORO256STARSTAR][idist(xoroshiro256starstar_i)];
            ++ibuckets[XORO256PLUS][idist(xoroshiro256plus_i)];
            ++ibuckets[XORO512STARSTAR][idist(xoroshiro512starstar_i)];
            ++ibuckets[XORO512PLUS][idist(xoroshiro512plus_i)];
            ++ibuckets[LINCONG][idist(linearcongruential_i)];
            ++ibuckets[RAN3][idist(ran3_i)];

            ++nbuckets[MTGEN][std::round(ndist(mtgen_n))];
            ++nbuckets[MSR][std::round(ndist(msr_n))];
            ++nbuckets[XOR128PLUS][std::round(ndist(xorshit128plus_n))];
            ++nbuckets[XOR1024STAR][std::round(ndist(xorshit1024star_n))];
            ++nbuckets[XORO128STARSTAR]
                      [std::round(ndist(xoroshiro128starstar_n))];
            ++nbuckets[XORO128PLUS][std::round(ndist(xoroshiro128plus_n))];
            ++nbuckets[XORO256STARSTAR]
                      [std::round(ndist(xoroshiro256starstar_n))];
            ++nbuckets[XORO256PLUS][std::round(ndist(xoroshiro256plus_n))];
            ++nbuckets[XORO512STARSTAR]
                      [std::round(ndist(xoroshiro512starstar_n))];
            ++nbuckets[XORO512PLUS][std::round(ndist(xoroshiro512plus_n))];
            ++nbuckets[LINCONG][std::round(ndist(linearcongruential_n))];
            ++nbuckets[RAN3][std::round(ndist(ran3_n))];
        }

        execute_test(test,
                     data,
                     ibuckets,
                     rbuckets,
                     nbuckets,
                     samplesize,
                     MTGEN,
                     failures,
                     test_confidence);
        execute_test(test,
                     data,
                     ibuckets,
                     rbuckets,
                     nbuckets,
                     samplesize,
                     MSR,
                     failures,
                     test_confidence);
        execute_test(test,
                     data,
                     ibuckets,
                     rbuckets,
                     nbuckets,
                     samplesize,
                     XOR128PLUS,
                     failures,
                     test_confidence);
        execute_test(test,
                     data,
                     ibuckets,
                     rbuckets,
                     nbuckets,
                     samplesize,
                     XOR1024STAR,
                     failures,
                     test_confidence);
        execute_test(test,
                     data,
                     ibuckets,
                     rbuckets,
                     nbuckets,
                     samplesize,
                     XORO128STARSTAR,
                     failures,
                     test_confidence);
        execute_test(test,
                     data,
                     ibuckets,
                     rbuckets,
                     nbuckets,
                     samplesize,
                     XORO128PLUS,
                     failures,
                     test_confidence);
        execute_test(test,
                     data,
                     ibuckets,
                     rbuckets,
                     nbuckets,
                     samplesize,
                     XORO256STARSTAR,
                     failures,
                     test_confidence);
        execute_test(test,
                     data,
                     ibuckets,
                     rbuckets,
                     nbuckets,
                     samplesize,
                     XORO256PLUS,
                     failures,
                     test_confidence);
        execute_test(test,
                     data,
                     ibuckets,
                     rbuckets,
                     nbuckets,
                     samplesize,
                     XORO512STARSTAR,
                     failures,
                     test_confidence);
        execute_test(test,
                     data,
                     ibuckets,
                     rbuckets,
                     nbuckets,
                     samplesize,
                     XORO512PLUS,
                     failures,
                     test_confidence);
        execute_test(test,
                     data,
                     ibuckets,
                     rbuckets,
                     nbuckets,
                     samplesize,
                     LINCONG,
                     failures,
                     test_confidence);
        execute_test(test,
                     data,
                     ibuckets,
                     rbuckets,
                     nbuckets,
                     samplesize,
                     RAN3,
                     failures,
                     test_confidence);
    }

    // doing a bit complicated mappings from names to booleans for
    // pretty printing failures and culprit generators
    bool failed = false;

    // this is actually higher than it should theoretically be, but 
    // we have random failures as well, hence the limit is chosen to be
    // on the same level as the built-in generators come out with.
    int  limit  = 3 * int(std::round((1. - test_confidence) * testnum));

    // map generator and kind to number of failures
    for (auto& [name, map] : failures)
    {
        for (auto& [kind, num_failure] : map)
        {
            std::cout << name << " in " << kind << " has " << num_failure << " failures when " << limit << " are allowed" << std::endl;
            if (num_failure > limit)
            {
                failed = true;
            }
        }
    }

    // pretty print if one has crossed failure threshold and for which of the
    // generators
    if (failed)
    {
        std::cerr << "The following generators failed the chi-square "
                     "distribution test"
                  << std::endl;
        for (auto& [name, map] : failures)
        {
            for (auto& [kind, num_failure] : map)
            {

                if (num_failure > limit)
                {
                    std::cerr << name << " in " << kind << " with "
                              << num_failure << " failures where " << limit
                              << " are allowed " << std::endl;
                }
            }
        }
        std::exit(-1);
    }
}

int
main()
{
    test_chisquare();
    return 0;
}
