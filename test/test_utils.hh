#ifndef MULTIRAND_TESTUTILS_HH
#define MULTIRAND_TESTUTILS_HH
#include <cmath>
#include <map>
#include <string>

namespace MultiRand
{
namespace Testutils
{
// enumeration for indexing -> useful to
// keep in mind which generator we are refering to
// etc
enum
{
    MTGEN           = 0,
    MSR             = 1,
    XOR128PLUS      = 2,
    XOR1024STAR     = 3,
    XORO128STARSTAR = 4,
    XORO128PLUS     = 5,
    XORO256STARSTAR = 6,
    XORO256PLUS     = 7,
    XORO512STARSTAR = 8,
    XORO512PLUS     = 9,
    LINCONG         = 10,
    RAN3            = 11
};

// map for telling which generator failed a test
std::map< int, std::string > names{ { 0, "MTGEN" },
                                    { 1, "MSR" },
                                    { 2, "XOR128PLUS" },
                                    { 3, "XOR1024STAR" },
                                    { 4, "XORO128STARSTAR" },
                                    { 5, "XORO128PLUS" },
                                    { 6, "XORO256STARSTAR" },
                                    { 7, "XORO256PLUS" },
                                    { 8, "XORO512STARSTAR" },
                                    { 9, "XORO512PLUS" },
                                    { 10, "LINCONG" },
                                    { 11, "RAN3" } };

/**
 * @brief Computes the probability that a random number is in the interval [A,B]
 *        when the underlying pdf is gaussian, see for the implemented solution:
 *
 http://www.wolframalpha.com/input/?i=integrate+(1%2Fsqrt(2*pi*1)*exp(-((x-0)%2F(2*1))%5E2)+dx+from+x%3DA+to+B
 *
 *
 * @param A lower bound of the interval
 * @param B upper bound of the interval

 * @return double P(x>= A && x <= B)
 */
double
P_interval_gauss(double A, double B)
{
    return (std::erf(B / std::sqrt(2)) - std::erf(A / std::sqrt(2))) / 2;
}

/**
 * @brief custom double compare for usage in map
 *
 */
class own_double_less
{
  public:

    /**
     * @brief Construct a new own double less object
     * 
     * @param arg_ 
     */
    own_double_less(double arg_ = 1e-7) : epsilon(arg_)
    {
    }

    /**
     * @brief less than for double
     * 
     * @param left 
     * @param right 
     * @return true 
     * @return false 
     */
    bool
    operator()(const double& left, const double& right) const
    {
        // you can choose other way to make decision
        // (The original version is: return left < right;)
        return (abs(left - right) > epsilon) && (left < right);
    }

    /**
     * @brief tolerance for considering one double less than another
     * 
     */
    double epsilon;
};

} // namespace Testutils
} // namespace MultiRand
#endif
