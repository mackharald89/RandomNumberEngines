import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sys
import os


# simple way to read command line args
# be careful to not read out [0], this is the script name
# argv[1] gives the path to the plotting
path = str(sys.argv[1])+'/'
if path == '/':
    path = ''  # set path to nothing if not given -> assumed that stuff lives in script' dir


# check if folders are there and make plot dir
if not os.path.exists(path+'plots/'):
    os.mkdir(path+'plots/')

    # plot benchmark stuff
fig, ax = plt.subplots(1, 1, figsize=(8, 6))
for postfix in ["gcc", "clang"]:
    data = pd.read_csv(
        path+'data/'+'times_for_real_numbers_in_μs_'+postfix+'.csv').T
    ax.plot(data, marker='o', label=postfix)
    ax.legend()
    ax.set_xticks(range(len(data)))
    ax.set_xticklabels(list(data.index))
plt.xticks(rotation=45)
ax.set_ylabel('time [μs]')
# ax.set_yscale('log', nonposy='clip')
fig.tight_layout()
fig.savefig(path+'plots/'+"runtimes_10^6_numbers.pdf", dpi=200)
plt.close('all')


# plot histograms
for postfix in ["gcc", "clang"]:
    for name, data in zip(['integer', 'real_valued', 'normal_distribution'],
                          [pd.read_csv(path+'data/'+"idata_"+postfix+".csv"),
                           pd.read_csv(path+'data/'+"rdata_"+postfix+".csv"),
                           pd.read_csv(path+'data/'+"ndata_"+postfix+".csv")]):
        if name == "normal_distribution":
            b = 50
        else:
            b = 9
        ax = data.hist(bins=b, figsize=(10, 8))
        for axis in ax.flat:
            axis.set_xlim([-4.2, 4.2])
        fig = ax.flat[0].get_figure()
        fig.tight_layout()
        fig.savefig(path+'plots/'+name+'_histogram_'+postfix+'.pdf', dpi=200)
        plt.close('all')


# plot spectral test for linear congruential generators - there output lies on
# 2d or higher hyperplanes, and the plots can in principle make them visible.
# If they are, the respective generator is shit
# careful, this takes a while
for postfix in ['gcc', 'clang']:
    idata = pd.read_csv(path+'data/'+'idata_'+postfix+'.csv')
    rdata = pd.read_csv(path+'data/'+'rdata_'+postfix+'.csv')
    for name, data in zip(['integer', 'real_valued'], [idata, rdata]):
        fig = plt.figure(figsize=(30, 30))
        for i, col in enumerate(data.columns):
            d = data[col].values.reshape((len(data)//3, 3))[0:5000:1]
            ax = fig.add_subplot(4, 4, i+1, projection='3d')
            ax.scatter3D(d[:, 0], d[:, 1], d[:, 2], s=5)
            ax.set_title(col)
        fig.savefig(path+'plots/'+name+'_spectral_'+postfix+'.pdf', dpi=200)
        plt.close('all')
