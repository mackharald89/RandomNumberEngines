#include "../include/chisquare.hh"
#include "../test/test_utils.hh"
#include <cmath>
#include <distributions.hh>
#include <numeric>
#include <random>
#include <random_number_engines.hh>
#include <type_traits>
#include <vector>

using namespace MultiRand;
using namespace MultiRand::Testutils;

/**
 * This function executes chi-square test for a
 *
 * @return [description]
 */
template < typename Idist, typename Rdist, typename Ndist >
auto
test_distributions(std::size_t testnum         = 100,
                   std::size_t samplesize      = 1000000,
                   double      test_confidence = 0.95,
                   std::size_t init_seed       = 62347890)
{
    ChiSquareTest test;
    // test for all generators

    std::default_random_engine             eng(init_seed);
    std::uniform_int_distribution< short > seed_dist(
        0, std::numeric_limits< short >::max());

    std::map< std::string, int >                     failures{ { "integer", 0 },
                                           { "real", 0 },
                                           { "normal", 0 } };
    std::map< int, std::size_t >                     ibuckets;
    std::map< double, std::size_t, own_double_less > rbuckets;
    std::map< double, std::size_t, own_double_less > nbuckets;

    Idist                                                idist(-4, 4);
    Rdist                                                rdist(-4., 4.);
    Ndist                                                ndist(0., 1.);
    std::vector< std::pair< std::size_t, std::size_t > > data;

    for (std::size_t l = 0; l < testnum; ++l)
    {
        std::cout << "currently at test number: " << l << std::endl;
        short        seed = seed_dist(eng);
        std::mt19937 mtgen_i(seed);
        std::mt19937 mtgen_r(seed);
        std::mt19937 mtgen_n(seed);

        for (std::size_t i = 0; i < samplesize; ++i)
        {
            ++ibuckets[idist(mtgen_i)];
            ++rbuckets[std::floor(rdist(mtgen_r))];
            ++nbuckets[std::round(ndist(mtgen_n))];
        }

        // now execute tests

        // for integer uniform dist
        data.resize(ibuckets.size());
        for (auto [it, vit] = std::make_pair(ibuckets.begin(), data.begin());
             it != ibuckets.end();
             ++it, ++vit)
        {
            *vit = std::make_pair(samplesize / ibuckets.size(), it->second);
        }

        if (test.evaluate(data, test_confidence, ibuckets.size() - 1) == false)
        {
            failures["integer"] += 1;
        }

        data.clear();

        // for real valued uniform dist
        data.resize(rbuckets.size());
        for (auto [it, vit] = std::make_pair(rbuckets.begin(), data.begin());
             it != rbuckets.end();
             ++it, ++vit)
        {
            *vit = std::make_pair(samplesize / rbuckets.size(), it->second);
        }

        if (test.evaluate(data, test_confidence, rbuckets.size() - 1) == false)
        {
            failures["real"] += 1;
        }
        data.clear();

        data.resize(nbuckets.size());
        for (auto [it, vit] = std::make_pair(nbuckets.begin(), data.begin());
             it != nbuckets.end();
             ++it, ++vit)
        {
            int num =
                std::round(P_interval_gauss(it->first - 0.5, it->first + 0.5) *
                           samplesize);

            *vit = std::make_pair(num, it->second);
        }

        if (test.evaluate(data, test_confidence, nbuckets.size() - 1) == false)
        {
            failures["normal"] += 1;
        }

        // clear data again
        ibuckets.clear();
        rbuckets.clear();
        nbuckets.clear();
        data.clear();
    }

    // map generator and kind to number of failures
    return std::make_pair(failures,
                          int(std::round((1. - test_confidence) * testnum)));
}

int
main()
{

    /*
    Idea behind tests: The chi-square test may fail, but what we are interested
    in is, since we are building an alternative to the stl, if the number of
    rejections we get with our custom distributions is comparable to the one
    we get with the stl distributions. Hence, we should not test only for
    failures but compare the number of failures with each other. This is done
    below.
    The test is considered passed if the number of failures for custom are
    not more than floor(3*#failures_with_stl) times and below 3 times the
    limit given.
    The numbers are somewhat random, but we need the same order of magnitude
    for stl & custom I think? (Or how should this be tested then)
    */

    auto [std_failures, std_lim] =
        test_distributions< std::uniform_int_distribution< int >,
                            std::uniform_real_distribution< double >,
                            std::normal_distribution< double > >();

    auto [custom_failures, custom_lim] =
        test_distributions< uniform_int_distribution< int >,
                            uniform_real_distribution< double >,
                            normal_distribution< double > >();

    auto std_it    = std_failures.begin();
    auto custom_it = custom_failures.begin();

    std::cout << std_lim << ", " << custom_lim << std::endl;

    for (; std_it != std_failures.end() && custom_it != custom_failures.end();
         ++std_it, ++custom_it)
    {
        std::cout << "std: " << std_it->first << ": " << std_it->second
                  << "; custom: " << custom_it->first << ": "
                  << custom_it->second << std::endl;
        if ((custom_it->second > floor(3 * std_it->second)) or
            (custom_it->second >= 3 * custom_lim))
        {
            std::cerr << "Test failed!" << std::endl;
            return 1;
        }
    }
    return 0;
}
