# --- TESTS ---
# All tests registered with 'add_test' will be run when calling 'make test'
#
# NOTE: CTest does *not* compile its test executables. Running 'make test'
# *without* compiling the appropriate executable will cause the test to fail!
#

# Conditionally add coverage flags to targets if CPP_COVERAGE is enabled
function(add_coverage_flags)
    foreach(arg ${ARGV})
        target_compile_options(${arg}
            PRIVATE
                $<$<BOOL:${CPP_COVERAGE}>:--coverage>
        )
        target_link_libraries(${arg}
            PRIVATE
                $<$<BOOL:${CPP_COVERAGE}>:--coverage>
        )
    endforeach()
endfunction()

# add tests as regular executables
add_executable(random_number_engines_test random_number_engines_test.cc)
add_executable(random_distributions_test random_distributions_test.cc)

# link the library to them
target_link_libraries(random_number_engines_test
    # can stay private, this target is never exported
    PRIVATE
        multirand
)
target_link_libraries(random_distributions_test
    # can stay private, this target is never exported
    PRIVATE
        multirand
)

# Add the coverage flags if enabled
add_coverage_flags(random_number_engines_test
                   random_distributions_test)

# register the test in CTest
add_test(NAME test-random-number-engines
    # automatically replaces target name with executable location
    COMMAND random_number_engines_test
)

add_test(NAME test-random-distributions
    # automatically replaces target name with executable location
    COMMAND random_distributions_test
)
