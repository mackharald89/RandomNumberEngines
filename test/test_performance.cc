#include "../include/chisquare.hh"
#include <iostream>
#include <random>
#include <random_number_engines.hh>

using namespace MultiRand;

int
main()
{
    std::cout << "test_performance" << std::endl;
    uint64_t    seed       = 795243;
    std::size_t samplesize = 1000000;

    std::mt19937         mtgen(seed);
    std::minstd_rand     msr(seed);
    XorShift128plus      xorshit128plus(seed);
    XorShift1024star     xorshit1024star(seed);
    SplitMix64           splitmix64(seed);
    Xoroshiro128starstar xoroshiro128starstar(seed);
    Xoroshiro128plus     xoroshiro128plus(seed);
    Xoroshiro256starstar xoroshiro256starstar(seed);
    Xoroshiro256plus     xoroshiro256plus(seed);
    Xoroshiro512starstar xoroshiro512starstar(seed);
    Xoroshiro512plus     xoroshiro512plus(seed);
    LinearCongruential<> linearcongruential(seed);
    Ran2                 ran2(seed);
    Ran3                 ran3(seed);

    uniform_int_distribution< int >     idist(0, 10);
    uniform_real_distribution< double > rdist(-5., 5.);
    normal_distribution< double >       ndist(0., 1.);

    std::ofstream timefile(
        "../../test/data/times_for_real_numbers_in_μs_" +
        (POSTFIX == 'G' ? std::string("gcc") : std::string("clang")) + ".csv");

    timefile
        << "mt19937,minstd_rand,XorShift128plus,XorShift1024star,SplitMix64,"
           "Xoroshiro128starstar,Xoroshiro128plus,"
           "Xoroshiro256starstar,Xoroshiro256plus,Xoroshiro512starstar,"
           "Xoroshiro512plus,LinearCongruential,Ran2,Ran3"
        << std::endl;

    std::vector< std::vector< double > > rdata(14);
    std::vector< long long >             timedata(14);
    std::chrono::high_resolution_clock   clock;

    for (std::size_t j = 0; j < 14; ++j)
    {
        rdata[j].resize(samplesize);
    }

    auto start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[MTGEN][i] = rdist(mtgen);
    }
    auto end = clock.now();
    timedata[MTGEN] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[MSR][i] = rdist(msr);
    }
    end = clock.now();
    timedata[MSR] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[XOR128PLUS][i] = rdist(xorshit128plus);
    }
    end = clock.now();
    timedata[XOR128PLUS] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[XOR1024STAR][i] = rdist(xorshit1024star);
    }
    end = clock.now();
    timedata[XOR1024STAR] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[SPLITMIX64][i] = rdist(splitmix64);
    }
    end = clock.now();
    timedata[SPLITMIX64] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[XORO128STARSTAR][i] = rdist(xoroshiro128starstar);
    }
    end = clock.now();
    timedata[XORO128STARSTAR] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[XORO128PLUS][i] = rdist(xoroshiro128plus);
    }
    end = clock.now();
    timedata[XORO128PLUS] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[XORO256STARSTAR][i] = rdist(xoroshiro256starstar);
    }
    end = clock.now();
    timedata[XORO256STARSTAR] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[XORO256PLUS][i] = rdist(xoroshiro256plus);
    }
    end = clock.now();
    timedata[XORO256PLUS] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[XORO512STARSTAR][i] = rdist(xoroshiro512starstar);
    }

    end = clock.now();
    timedata[XORO512STARSTAR] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[XORO512PLUS][i] = rdist(xoroshiro512plus);
    }
    end = clock.now();
    timedata[XORO512PLUS] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[LINCONG][i] = rdist(linearcongruential);
    }
    end = clock.now();
    timedata[LINCONG] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[RAN2][i] = rdist(ran2);
    }
    end = clock.now();
    timedata[RAN2] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    start = clock.now();
    for (std::size_t i = 0; i < samplesize; ++i)
    {
        rdata[RAN3][i] = rdist(ran3);
    }
    end = clock.now();
    timedata[RAN3] =
        std::chrono::duration_cast< std::chrono::microseconds >(end - start)
            .count();

    timefile << timedata[MTGEN] << "," << timedata[MSR] << ","
             << timedata[XOR128PLUS] << "," << timedata[XOR1024STAR] << ","
             << timedata[SPLITMIX64] << "," << timedata[XORO128STARSTAR] << ","
             << timedata[XORO128PLUS] << "," << timedata[XORO256STARSTAR] << ","
             << timedata[XORO256PLUS] << "," << timedata[XORO512STARSTAR] << ","
             << timedata[XORO512PLUS] << "," << timedata[LINCONG] << ","
             << timedata[RAN2] << "," << timedata[RAN3] << std::endl;

    return 0;
}
